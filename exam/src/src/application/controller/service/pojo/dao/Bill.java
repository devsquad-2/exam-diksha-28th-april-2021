package application.controller.service.pojo.dao;

import java.util.Date;

public class Bill {
    private double payableAmount;
    private Date dueDate;

    public Bill() {
        payableAmount = 0;
        dueDate = new Date();
    }

    public Bill(double payableAmount, Date dueDate) {
        this.payableAmount = payableAmount;
        this.dueDate = dueDate;
    }

    public double getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(double payableAmount) {
        this.payableAmount = payableAmount;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String toString() {
        return "Due Date: "+getDueDate().toString()+", Due Amount: "+getPayableAmount();
    }
}
