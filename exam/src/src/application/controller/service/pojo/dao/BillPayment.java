package application.controller.service.pojo.dao;

import java.util.Date;
import java.util.Scanner;

public class BillPayment {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);

        String username, password;
        String emailRegex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        String passwordRegix = "^[a-zA-Z0-9@\\\\#$%&*()_+\\]\\[';:?.,!^-]{8,}$";
        int day,month,year;
        double penalty = 0;
        Bill bill = new Bill(80.0, new Date(2021, 04, 30));

        try{
            System.out.println("Enter username: ");
            username = scanner.nextLine();
            System.out.println("Enter password: ");
            password = scanner.nextLine();

            if(!username.matches(emailRegex) || !password.matches(passwordRegix)) {
                System.out.println("Sorry invalid Credentials");
                System.exit(-1);
            }
            else
            {
                System.out.println("Successfully Login");
            }

            System.out.println(bill.toString()+"\n");

            System.out.println("Enter bill payment date (today's date): ");
            System.out.print("Enter Day: ");
            day = scanner.nextInt();
            System.out.print("Enter Month: ");
            month = scanner.nextInt();
            System.out.print("Enter Year: ");
            year = scanner.nextInt();

            Date today = new Date(year, month, day);

            if(bill.getDueDate().compareTo(today) < 0) {
                System.out.println("Due date passed!");
                penalty = 0.005;
            }

            double payableAmount = bill.getPayableAmount() + (bill.getPayableAmount() * penalty);
            System.out.println("Payable amount is: "+payableAmount);

            System.out.print("Enter amount: ");
            double amount = scanner.nextDouble();

            if(amount >= payableAmount) {
                System.out.println("You have paid due bill. Your change is " + (amount - payableAmount));
            }
            else {
                System.out.println("Your amount is insufficient.");
            }

        }catch (NumberFormatException ex1 ) {
            System.err.println(ex1.getMessage());
        } catch (Exception ex2) {
            System.err.println(ex2.getMessage());
        }
    }
}
